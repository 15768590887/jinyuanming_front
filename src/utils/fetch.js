import axios from "axios";
import { Message, MessageBox } from "element-ui";
import store from "../store";
import { getToken, removeToken } from "utils/auth";

// 创建axios实例
const service = axios.create({
  // baseURL: "http://2ix1205033.imwork.net", // api的base_url
  //baseURL: "http://47.107.189.128:8765", // 从环境进程中根据运行环境获取的api的base_url
  baseURL: "http://127.0.0.1:8765",
  timeout: 5000 // 请求超时时间
});

// request拦截器
service.interceptors.request.use(
  config => {
    // Do something before request is sent
    if (store.getters.token) {
      config.headers["Authorization"] = getToken(); // 让每个请求携带token--['X-Token']为自定义key 请根据实际情况自行修改
    }
    return config;
  },
  error => {
    // Do something with request error
    console.log(error); // for debug
    Promise.reject(error);
  }
);

// respone拦截器
service.interceptors.response.use(
  response => {
    /**
     * 下面的注释为通过response自定义code来标示请求状态，当code返回如下情况为权限有问题，登出并返回到登录页
     * 如通过xmlhttprequest 状态码标识 逻辑可写在下面error中
     */
    const res = response.data;
    console.log(response);
    if (response.status === 500 || res.status === 5000) {
      console.log(res.message);
      Message({
        message: "系统繁忙，稍后重试",
        type: "error",
        duration: 2 * 1000
      });
      return Promise.reject("error");
    }
    if (
      response.status === 401 ||
      res.status === 40101 ||
      res.status === 40301
    ) {
      MessageBox.confirm(
        "登录已过期，你可以取消继续留在该页面，或者重新登录",
        "登录过期",
        {
          confirmButtonText: "重新登录",
          cancelButtonText: "取消",
          type: "warning"
        }
      ).then(() => {
        store.dispatch("FedLogOut").then(() => {
          location.reload(); // 为了重新实例化vue-router对象 避免bug
        });
      });
      return Promise.reject("error");
    }
    // if (res.status === 40301) {
    //   Message({
    //     message: res.message,
    //     type: "error",
    //     duration: 5 * 1000
    //   });
    //   return Promise.reject("error");
    // }
    if (res.status === 40001 || res.status === 40002) {
      console.log(response);
      Message({ message: res.message, type: "warning" });
      if (
        res.message === "用户不存在或账户密码错误!" ||
        res.message === "验证不通过或已失效，请刷新重试"
      ) {
        var reload = function() {
          location.reload();
        };
        window.setTimeout(reload, 1500);
      }
      return Promise.reject("error");
    }
    if (res.status === 40000) {
      Message({
        message: res.message,
        type: "error"
      });
      return Promise.reject("error");
    }
    if (response.status !== 200 && res.status !== 200) {
      Message({
        message: res.message,
        type: "error",
        duration: 2 * 1000
      });
    } else {
      return response.data;
    }
  },
  error => {
    // console.log(error); // for debug
    Message({
      message: error.message,
      type: "error",
      duration: 2 * 1000
    });
    return Promise.reject(error);
  }
);

export default service;

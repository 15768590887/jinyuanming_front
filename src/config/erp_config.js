/**
 *
 *ERP系统配置
 *
 */
export const config = {
 


  //  baseURL: "http://127.0.0.1:8765",
  baseURL: "http://47.107.103.227:8765",
  // xlsUrl: "http://wq.chayingwh.com/baojia/",


  //API请求超时时间
  timeout: 5000,

  //ERP系统标题
  companyName: "化工厂报价管理系统",

  //是否开启全屏功能
  fullScreenButton: true,

  //是否开启消息提醒功能
  messageButton: true,

  /**
   * 路由动画切换方式
   * @translate 水平切换
   * @fade 淡入淡出
   * 其余值或空值即没有动画
   */
  animateMode: "fade"
};
import Vue from "vue";
import Router from "vue-router";
const _import = require("./_import_" + process.env.NODE_ENV);
// in development env not use Lazy Loading,because Lazy Loading large page will cause webpack hot update too slow.so only in production use Lazy Loading

Vue.use(Router);

/* layout */
import Layout from "../views/layout/Layout";

/**
 * icon : the icon show in the sidebar
 * hidden : if `hidden:true` will not show in the sidebar
 * redirect : if `redirect:noredirect` will no redirct in the levelbar
 * noDropdown : if `noDropdown:true` will has no submenu
 * meta : { role: ['admin'] }  will control the page role
 **/
export const constantRouterMap = [
  {
    path: "/reg",
    component: _import("reg/index"),
    hidden: true
  },
  {
    path: "/login",
    component: _import("login/index"),
    hidden: true
  },
  {
    path: "/authredirect",
    component: _import("login/authredirect"),
    hidden: true
  },
  {
    path: "/404",
    component: _import("error/404"),
    hidden: true
  },
  {
    path: "/401",
    component: _import("error/401"),
    hidden: true
  },
  {
    path: "/",
    component: Layout,
    redirect: "/dashboard",
    name: "首页",
    hidden: true,
    children: [
      {
        path: "dashboard",
        component: _import("dashboard/index")
      }
    ]
  },
  {
    path: "/introduction",
    component: Layout,
    redirect: "/introduction/index",
    icon: "form",
    noDropdown: true,
    children: [
      {
        path: "index",
        component: _import("introduction/index"),
        name: "简述"
      }
    ]
  }
];

export default new Router({
  // mode: 'history', //后端支持可开
  scrollBehavior: () => ({
    y: 0
  }),
  base: __dirname,
  routes: constantRouterMap
});

export const asyncRouterMap = [
  {
    path: "/userTestManager",
    component: Layout,
    name: "用户测试管理",
    icon: "setting",
    authority: "userTestManager",
    children: [
      /**一级菜单 */
      {
        path: "menu/:id",
        component:  _import("ilearn/usertest/index"),
        name: "menu",
        authority: "userTestManager"
      },
      /*
      二级菜单
      {
        path: "menu/:id",
        component: _import("menu/index"),
        name: "menu",
        authority: "UserTestManager"
      },
      {
        path: "UserTestManager",
        icon: "userM",
        component: _import("ilearn/usertest/index"),
        name: "用户测试管理",
        authority: "UserTestManager"
      }, 
      */
    ]
  },
  {
    path: "/supplierRegcodeManager",
    component: Layout,
    name: "供应商注册码",
    icon: "setting",
    authority: "supplierRegcodeManager",
    children: [
      {
        path: "menu/:id",
        component:  _import("jinyuanming/supplierRegcode/index"),
        name: "menu",
        authority: "supplierRegcodeManager"
      },
    ]
  },
  {
    path: "/customerRegcodeManager",
    component: Layout,
    name: "设备客户码",
    icon: "setting",
    authority: "customerRegcodeManager",
    children: [
      {
        path: "menu/:id",
        component:  _import("jinyuanming/customerRegcode/index"),
        name: "menu",
        authority: "customerRegcodeManager"
      },
    ]
  },

  {
    path: "/baseManager",
    component: Layout,
    name: "系统管理",
    icon: "setting",
    authority: "baseManager",
    children: [
      {
        path: "menu/:id",
        component: _import("menu/index"),
        name: "menu",
        authority: "baseManager"
      },
      {
        path: "userManager",
        icon: "userM",
        component: _import("admin/user/index"),
        name: "用户管理",
        authority: "userManager"
      },
      {
        path: "menuManager",
        icon: "category",
        component: _import("admin/menu/index"),
        name: "菜单管理",
        authority: "menuManager"
      },
      {
        path: "groupManager",
        icon: "roleM",
        component: _import("admin/group/index"),
        name: "角色权限",
        authority: "groupManager"
      },
      {
        path: "groupTypeManager",
        icon: "roleM",
        component: _import("admin/groupType/index"),
        name: "角色类型",
        authority: "groupTypeManager"
      },
      {
        path: "gateLogManager",
        icon: "logM",
        component: _import("admin/gateLog/index"),
        name: "操作日志",
        authority: "gateLogManager"
      },
      {
        path: "companyManager",
        icon: "businessM",
        component: _import("admin/company/index"),
        name: "企业管理",
        authority: "companyManager"
      },
      {
        path: "departManager",
        icon: "departmentalM",
        component: _import("admin/depart/index"),
        name: "部门管理",
        authority: "departManager"
      },
      {
        path: "dictManager",
        icon: "dictionaryM",
        component: _import("admin/dict/index"),
        name: "字典管理",
        authority: "dictManager"
      },
      {
        path: "staffManager",
        icon: "staffM",
        component: _import("admin/staff/index"),
        name: "员工管理",
        authority: "staffManager"
      },
      {
        path: "paramManager",
        icon: "staffM",
        component: _import("admin/param/index"),
        name: "参数设置",
        authority: "paramManager"
      },
      {
        path: "functionManager",
        icon: "staffM",
        component: _import("function/index"),
        name: "数学公式",
        authority: "functionManager"
      },
      {
        path: "taskManager",
        icon: "staffM",
        component: _import("admin/task/index"),
        name: "任务调度",
        authority: "taskManager"
      },
      {
        path: "personalCentre",
        icon: "personalCentre",
        component: _import("admin/personalCentre/index"),
        name: "个人中心",
        authority: "baseManager"
      },
      {
        path: "changePassword",
        icon: "changePassword",
        component: _import("admin/changePassword/index"),
        name: "修改密码",
        authority: "baseManager" //访问权限
      },
      {
        path: "messageReminder",
        icon: "messageReminder",
        component: _import("admin/messageReminder/index"),
        name: "消息提醒",
        authority: "baseManager"
      }
    ]
  }
];

// {
//   path: '/monitorManager',
//   component: Layout,
//   name: '监控模块管理',
//   icon: 'setting',
//   authority: 'monitorManager',
//   children: [{
//     path: 'serviceEurekaManager',
//     component: _import('monitor/eureka/index'),
//     name: 'Eureka注册中心',
//     authority: 'serviceEurekaManager'
//   }, {
//     path: 'serviceMonitorManager',
//     component: _import('monitor/service/index'),
//     name: '服务状态监控',
//     authority: 'serviceMonitorManager'
//   }, {
//     path: 'serviceZipkinManager',
//     component: _import('monitor/zipkin/index'),
//     name: '服务状态监控',
//     authority: 'serviceZipkinManager'
//   }]
// }

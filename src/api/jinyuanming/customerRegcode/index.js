import fetch from 'utils/fetch';

export function page(query) {
return fetch({
url: '/jinyuanming/customerRegcode/page',
method: 'get',
params: query
});
}

export function addObj(obj) {
return fetch({
url: '/jinyuanming/customerRegcode',
method: 'post',
data: obj
});
}

export function getObj(id) {
return fetch({
url: '/jinyuanming/customerRegcode/' + id,
method: 'get'
})
}

export function delObj(id) {
return fetch({
url: '/jinyuanming/customerRegcode/' + id,
method: 'delete'
})
}

export function putObj(id, obj) {
return fetch({
url: '/jinyuanming/customerRegcode/' + id,
method: 'put',
data: obj
})
}

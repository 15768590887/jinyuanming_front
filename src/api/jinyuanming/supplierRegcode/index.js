import fetch from 'utils/fetch';

export function page(query) {
return fetch({
url: '/jinyuanming/supplierRegcode/page',
method: 'get',
params: query
});
}

export function addObj(obj) {
return fetch({
url: '/jinyuanming/supplierRegcode',
method: 'post',
data: obj
});
}

export function getObj(id) {
return fetch({
url: '/jinyuanming/supplierRegcode/' + id,
method: 'get'
})
}

export function delObj(id) {
return fetch({
url: '/jinyuanming/supplierRegcode/' + id,
method: 'delete'
})
}

export function putObj(id, obj) {
return fetch({
url: '/jinyuanming/supplierRegcode/' + id,
method: 'put',
data: obj
})
}

import fetch from "utils/fetch";

export function page(query) {
return fetch({
url: '/ilearn/usertest/page',
method: 'get',
params: query
});
}

export function addObj(obj) {
return fetch({
url: '/ilearn/usertest',
method: 'post',
data: obj
});
}

export function getObj(id) {
return fetch({
url: '/ilearn/usertest/' + id,
method: 'get'
})
}

export function delObj(id) {
return fetch({
url: '/ilearn/usertest/' + id,
method: 'delete'
})
}

export function putObj(id, obj) {
return fetch({
url: '/ilearn/usertest/' + id,
method: 'put',
data: obj
})
}

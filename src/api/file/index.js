import fetch from "utils/fetch";

/**
 * 创建文件夹
 *
 * @param fileInfo.name     String 文件夹名
 * @param fileInfo.parentId String 可选，上级目录ID，默认为根目录
 */
export function mkdir(fileInfo) {
  return fetch({
    url: "/fs/folder",
    method: "POST",
    data: fileInfo
  });
}

/**
 * 上传文件
 * FormData是浏览器内置对象，使用示例：
 * let formData = new FormData();
 * formData.append("file", files[0]);
 *
 * @param formData.append("file", [文件流对象])
 * @param formData.append("parentId", [所属目录ID])
 */
export function touch(formData) {
  return fetch({
    url: "/fs/file",
    method: "POST",
    data: formData
  });
}

/**
 * 文件或文件夹重命名
 *
 * @param fileInfo.id     String 你要改哪个文件信息？
 * @param fileInfo.name   String 你要改成什么名字？（注意：后缀是不能改的）
 */
export function rename(fileInfo) {
  return fetch({
    url: "/fs/file/name",
    method: "PUT",
    data: fileInfo
  });
}

/**
 * 删除文件或文件夹（实际上是做隐藏，并没从磁盘上移除）
 *
 * @param id  String 你要删哪个文件信息？
 */
export function remove(id) {
  return fetch({
    url: `/fs/file?id=${id}`,
    method: "DELETE"
  });
}

/**
 * 获得文件或文件夹下载地址，下载文件要先获得地址再通过按钮事件下载，原因是要检查当前用户是否有权限获取当前的文件。
 * 如果是文件夹下载，则下载的是一个zip压缩包，里面装的该目录下的所有文件和文件夹
 *
 * @param id    String  你要获取哪个文件信息的下载地址？
 * @param force Boolean 可选，是否需要强制下载，即不管当前用户是谁，直接强行获得下载地址，该参数用于以超级管理员的身份。
 */
export function getDownloadUrl(id, force) {
  return fetch({
    url: `/fs/file/downloadUrl?id=${id}${force? "&force=true" : ""}`,
    method: "GET"
  });
}

/**
 * 文件共享，分享给单个人，即只是在分享用户集合里新增一个人的ID
 *
 * @param fileInfo.id     String 哪个文件？
 * @param fileInfo.userId String 要分享的用户ID
 */
export function share(fileInfo) {
  return fetch({
    url: `/fs/file/usersId/${fileInfo.userId}?id=${fileInfo.id}`,
    method: "PUT"
  });
}

/**
 * 文件共享，用一个集合替换先前的分享集合
 *
 * @param fileInfo.id       String  哪个文件？
 * @param fileInfo.usersId  Array   分享用户ID集合
 */
export function shareAll(fileInfo) {
  return fetch({
    url: "/fs/file/usersId",
    method: "PUT",
    data: fileInfo
  });
}

/**
 * 查询单个文件信息
 *
 * @param id    String  哪个文件？
 * @param force Boolean 可选，是否需要强制查看，无论当前用户是谁？
 */
export function queryOne(id, force) {
  return fetch({
    url: `/fs/file?id=${id}${force? "&force=true" : ""}`,
    method: "GET"
  });
}

/**
 * 查询与我相关的文件列表
 *
 * @param force Boolean 可选，是否需要强制查看所有用户的文件列表？默认为只查与当前用户相关的。
 */
export function query(force) {
  return fetch({
    url: `/fs/files${force? "?force=true" : ""}`,
    method: "GET"
  });
}

/**
 * 获取文件上传限制的最大值
 */
export function getFileMaxSize() {
  return fetch({
    url: "/fs/file/maxsize",
    method: "GET"
  });
}

import fetch from "utils/fetch";

export function findAll() {
	return fetch({
		url: "/functions",
		method: "GET"
	});
}

/**
 * 调用该函数时，在回调promise的then里会传入一个calculator计算器
 * 获得该计算器可以调用其calculate计算函数进行选取指定的函数表达式进行计算
 * 比如:
 * 	querySelector("一次函数", "指数函数").then(calculator => {
 * 		// 把calculator赋值进vue的data里
 *  	this.calculator = calculator;
 * 		// 计算例子，注意可变参数
 * 		calculator.calculate("一次函数", 5, 6, 7);
 *  })
 * 
 * @param  {...any} args 你要查询哪几条函数表达式？
 */
export function querySelector(...args) {
	let map = new Map();
	return new Promise((resolve, reject) => {
		fetch({
			url: `/functions/_filter?names=${args.reduce((total, currentValue) => total + "," + currentValue)}`,
			method: "GET"
		}).then(res => {
			Reflect.ownKeys(res).forEach(key => map.set(key, Reflect.get(res, key)));
			resolve({
				"calculate": (functionName, ...args) => calculate(map.get(functionName), ...args)
			})
		}).catch(res => reject(res));
	});
}

export function save(obj) {
	return fetch({
		url: "/functions",
		method: "POST",
		data: obj
	});
}

export function remove(id) {
	return fetch({
		url: `/functions/${id}`,
		method: "DELETE"
	});
}

export function findByName(name) {
	return fetch({
		url: `/functions/${encodeURIComponent(name)}`,
		method: "GET"
	});
}

export function calculate(exp, ...args) {
	if (exp && exp.formalParameters && exp.expression) {
		return eval(`${exp.formalParameters} => ${exp.expression}`)(...args);
	}
}


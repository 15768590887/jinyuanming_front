import fetch from "utils/fetch";

export function findOne(id) {
  return fetch({
		url: `/erp/depots/${id}`,
		method: "GET"
	});
}

export function findPage(query) {
	return fetch({
		url: "/erp/depots/page",
		method: "GET",
		params: query
	});
}

export function findAll() {
	return fetch({
		url: "/erp/depots",
		method: "GET"
	});
}

export function findLocators(id) {
	return fetch({
		url: `/erp/depots/${id}/locators`,
		method: "GET"
	});
}

export function save(depot) {
	return fetch({
		url: "/erp/depots",
		method: "POST",
		data: depot
	});
}

export function remove(id) {
	return fetch({
		url: `/erp/depots/${id}`,
		method: "DELETE"
	});
}

export function input(bales) {
	return fetch({
		url: "/erp/depots/bales",
		method: "POST",
		data: bales
	});
}


import fetch from "utils/fetch";

export function page(query) {
  return fetch({
    url: "/erp/storage/goodsSpec/page",
    method: "get",
    params: query
  });
}

export function addObj(obj) {
  return fetch({
    url: "/erp/storage/goodsSpec",
    method: "post",
    data: obj
  });
}

export function getObj(id) {
  return fetch({
    url: "/erp/storage/goodsSpec/" + id,
    method: "get"
  });
}

export function delObj(id) {
  return fetch({
    url: "/erp/storage/goodsSpec/" + id,
    method: "delete"
  });
}

export function putObj(id, obj) {
  return fetch({
    url: "/erp/storage/goodsSpec/" + id,
    method: "put",
    data: obj
  });
}

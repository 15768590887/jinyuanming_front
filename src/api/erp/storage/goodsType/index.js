import fetch from "utils/fetch";

export function page(query) {
  return fetch({
    url: "/erp/storage/goodsType/page",
    method: "get",
    params: query
  });
}

export function addObj(obj) {
  return fetch({
    url: "/erp/storage/goodsType",
    method: "post",
    data: obj
  });
}

export function getObj(id) {
  return fetch({
    url: "/erp/storage/goodsType/" + id,
    method: "get"
  });
}

export function delObj(id) {
  return fetch({
    url: "/erp/storage/goodsType/" + id,
    method: "delete"
  });
}

export function putObj(id, obj) {
  return fetch({
    url: "/erp/storage/goodsType/" + id,
    method: "put",
    data: obj
  });
}

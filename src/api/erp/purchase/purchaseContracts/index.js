import fetch from "utils/fetch";

export function addObj(obj) {
  return fetch({
    url: "/erp/purchase",
    method: "post",
    data: obj
  });
}

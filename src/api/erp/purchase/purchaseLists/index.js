import fetch from "utils/fetch";

export function code() {
  return fetch({
    url: "/erp/purchase/contract/code",
    method: "get"
  });
}

export function page(query) {
  return fetch({
    url: "/erp/purchase/contract/page",
    method: "get",
    params: query
  });
}

export function pageSelf(query) {
  return fetch({
    url: "/erp/purchase/contract/self/page",
    method: "get",
    params: query
  });
}

export function getObj(id) {
  return fetch({
    url: "/erp/purchase/contract/" + id,
    method: "get"
  });
}
export function getObjSelf(id) {
  return fetch({
    url: "/erp/purchase/contract/self/" + id,
    method: "get"
  });
}
export function delObj(id) {
  return fetch({
    url: "/erp/purchase/" + id,
    method: "delete"
  });
}

export function putObj(obj) {
  return fetch({
    url: "/erp/purchase/",
    method: "put",
    data: obj
  });
}

export function toPort(obj) {
  return fetch({
    url: "/erp/purchase/toport/weight/",
    method: "put",
    data: obj
  });
}
export function sign(obj) {
  return fetch({
    url: "/erp/purchase/sign/",
    method: "put",
    data: obj
  });
}

import fetch from "utils/fetch";

export function page(query) {
  return fetch({
    url: "/erp/purchase/freight/page",
    method: "get",
    params: query
  });
}

export function addObj(obj) {
  return fetch({
    url: "/erp/purchase/freight",
    method: "post",
    data: obj
  });
}

export function getObj(id) {
  return fetch({
    url: "/erp/purchase/freight/" + id,
    method: "get"
  });
}

export function delObj(id) {
  return fetch({
    url: "/erp/purchase/freight/" + id,
    method: "delete"
  });
}

export function putObj(obj) {
  return fetch({
    url: "/erp/purchase/freight/",
    method: "put",
    data: obj
  });
}

import fetch from "utils/fetch";

export function page(query) {
  return fetch({
    url: "/erp/purchase/ocean/page",
    method: "get",
    params: query
  });
}

export function addObj(obj) {
  return fetch({
    url: "/erp/purchase/ocean",
    method: "post",
    data: obj
  });
}

export function getObj(id) {
  return fetch({
    url: "/erp/purchase/ocean/" + id,
    method: "get"
  });
}

export function getObjByPurchase(id) {
  return fetch({
    url: "/erp/purchase/ocean/contract/" + id,
    method: "get"
  });
}

export function delObj(id, purchaseId) {
  return fetch({
    url: "/erp/purchase/ocean/" + id + "/" + purchaseId,
    method: "delete"
  });
}

export function putObj(obj) {
  return fetch({
    url: "/erp/purchase/ocean/",
    method: "put",
    data: obj
  });
}

import fetch from "utils/fetch";

export function page(query) {
  return fetch({
    url: "/erp/purchaser/page",
    method: "get",
    params: query
  });
}

export function addObj(obj) {
  return fetch({
    url: "/erp/purchaser",
    method: "post",
    data: obj
  });
}

export function getObj(id) {
  return fetch({
    url: "/erp/purchaser/" + id,
    method: "get"
  });
}

export function delObj(id) {
  return fetch({
    url: "/erp/purchaser/" + id,
    method: "delete"
  });
}

export function putObj(id, obj) {
  return fetch({
    url: "/erp/purchaser/" + id,
    method: "put",
    data: obj
  });
}
export function downExcel(obj) {
  return fetch({
    url: "/erp/purchaser/download",
    method: "post",
    data: obj
  });
}
export function uploadExcel(obj) {
  return fetch({
    url: "/erp/purchaser/upload",
    method: "post",
    data: obj,
    headers: {
      "Content-Type": "multipart/form-data"
    }
  });
}

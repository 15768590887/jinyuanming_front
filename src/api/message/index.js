import fetch from "utils/fetch";

export function read(id) {
  return fetch({
		url: `/messages/${id}/isRead`,
		method: "PUT"
	});
}

export function findNotReadCount() {
	return fetch({
		url: "/messages/notReadCounts",
		method: "GET"
	});
}

export function findList() {
	return fetch({
		url: "/messages",
		method: "GET"
	});
}

export function findOne(id) {
	return fetch({
		url: `/messages/${id}`,
		method: "GET"
	});
}

export function findPage(query) {
	return fetch({
		url: `/messages/page`,
		method: "GET",
		params: query
	});
}
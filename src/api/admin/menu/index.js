import fetch from "utils/fetch";

export function fetchTree(query) {
  return fetch({
    url: "/admin/menu/tree",
    method: "get",
    params: query
  });
}
export function fetchUserTree() {
  return fetch({
    url: "/admin/menu/user/authorityTree",
    method: "get"
  });
}
export function fetchAll() {
  return fetch({
    url: "/admin/menu/all",
    method: "get"
  });
}
export function addObj(obj) {
  return fetch({
    url: "/admin/menu",
    method: "post",
    data: obj
  });
}

export function getObj(id) {
  return fetch({
    url: "/admin/menu/" + id,
    method: "get"
  });
}

export function delObj(id) {
  return fetch({
    url: "/admin/menu/" + id,
    method: "delete"
  });
}

export function putObj(id, obj) {
  return fetch({
    url: "/admin/menu/" + id,
    method: "put",
    data: obj
  });
}

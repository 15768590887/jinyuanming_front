import fetch from "utils/fetch";

export function page(query) {
  return fetch({
    url: "/admin/demo/page",
    method: "get",
    params: query
  });
}

export function addObj(obj) {
  return fetch({
    url: "/demo/demo",
    method: "post",
    data: obj
  });
}

export function getObj(id) {
  return fetch({
    url: "/admin/demo/" + id,
    method: "get"
  });
}

export function delObj(id) {
  return fetch({
    url: "/admin/demo/" + id,
    method: "delete"
  });
}

export function putObj(id, obj) {
  return fetch({
    url: "/admin/demo/" + id,
    method: "put",
    data: obj
  });
}

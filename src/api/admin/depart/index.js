import fetch from "utils/fetch";

export function fetchTree(query) {
  return fetch({
    url: "/admin/depart/tree",
    method: "get",
    params: query
  });
}

export function page(query) {
  return fetch({
    url: "/admin/depart/page",
    method: "get",
    params: query
  });
}

export function addObj(obj) {
  return fetch({
    url: "/admin/depart",
    method: "post",
    data: obj
  });
}

export function getObj(id) {
  return fetch({
    url: "/admin/depart/" + id,
    method: "get"
  });
}

export function delObj(id) {
  return fetch({
    url: "/admin/depart/" + id,
    method: "delete"
  });
}

export function putObj(id, obj) {
  return fetch({
    url: "/admin/depart/" + id,
    method: "put",
    data: obj
  });
}

import fetch from "utils/fetch";

export function page(query) {
  return fetch({
    url: "/admin/user/page",
    method: "get",
    params: query
  });
}

export function departTree(query) {
  return fetch({
    url: "/admin/depart/tree",
    method: "get",
    params: query
  });
}
export function roleTree(query) {
  return fetch({
    url: "/admin/group/tree",
    method: "get",
    params: query
  });
}
export function departPage(query) {
  return fetch({
    url: "/admin/depart/page",
    method: "get",
    params: query
  });
}
export function compPage(query) {
  return fetch({
    url: "/admin/company/page",
    method: "get",
    params: query
  });
}

export function addObj(obj) {
  return fetch({
    url: "/admin/user",
    method: "post",
    data: obj
  });
}


export function getObj(id) {
  return fetch({
    url: "/admin/user/" + id,
    method: "get"
  });
}

export function delObj(id) {
  return fetch({
    url: "/admin/user/" + id,
    method: "delete"
  });
}

export function putObj(id, obj) {
  return fetch({
    url: "/admin/user/" + id,
    method: "put",
    data: obj
  });
}

import fetch from "utils/fetch";

export function page(query) {
  return fetch({
    url: "/admin/dict/page",
    method: "get",
    params: query
  });
}
export function getDictList(name) {
  return fetch({
    url: "/admin/dict/common/name/" + name,
    method: "get"
  });
}
export function addObj(obj) {
  return fetch({
    url: "/admin/dict",
    method: "post",
    data: obj
  });
}

export function getObj(id) {
  return fetch({
    url: "/admin/dict/" + id,
    method: "get"
  });
}

export function delObj(id) {
  return fetch({
    url: "/admin/dict/" + id,
    method: "delete"
  });
}

export function putObj(id, obj) {
  return fetch({
    url: "/admin/dict/" + id,
    method: "put",
    data: obj
  });
}

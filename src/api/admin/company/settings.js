import fetch from "utils/fetch";

export function save(obj) {
	return fetch({
		url: "/admin/company/setting",
		method: "PUT",
		data: obj
	});
}

export function load() {
	return fetch({
		url: "/admin/company/setting",
		method: "GET"
	});
}
import fetch from "utils/fetch";

export function getObj(token) {
  return fetch({
    url: "/admin/user/personal/" + token,
    method: "get"
  });
}

export function putObj(id, obj) {
  return fetch({
    url: "/admin/user/personal/" + id,
    method: "put",
    data: obj
  });
}

export function updatePass(obj) {
  return fetch({
    url: "/admin/user/personal/pass/",
    method: "put",
    data: obj
  });
}

import fetch from "utils/fetch";

export function save(job) {
  return fetch({
		url: "/jobs",
		method: "POST",
		data: job
	});
}

export function trigger(jobName, jobGroup) {
	return fetch({
		url: `/jobs/${jobName}/_trigger?jobGroup=${jobGroup}`,
		method: "PUT"
	});
}

export function pause(jobName, jobGroup) {
	return fetch({
		url: `/jobs/${jobName}/_pause?jobGroup=${jobGroup}`,
		method: "PUT"
	});
}

export function resume(jobName, jobGroup) {
	return fetch({
		url: `/jobs/${jobName}/_resume?jobGroup=${jobGroup}`,
		method: "PUT"
	});
}

export function remove(jobName, jobGroup) {
	return fetch({
		url: `/jobs/${jobName}?jobGroup=${jobGroup}`,
		method: "DELETE"
	});
}

export function findOne(jobName, jobGroup) {
	return fetch({
		url: `/jobs/${jobName}?jobGroup=${jobGroup}`,
		method: "GET"
	});
}

export function findPage(query) {
	return fetch({
		url: `/jobs`,
		method: "GET",
		params: query
	});
}
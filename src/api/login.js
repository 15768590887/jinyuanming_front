import fetch from "utils/fetch";

export function getPassPubKey() {
  return fetch({
    url: "/auth/jwt/passPubKey",
    method: "get"
  });
}
export function getCode() {
  return fetch({
    url: "/auth/jwt/identify/startCaptch?t=" + new Date().getTime(),
    method: "get"
  });
}
export function loginByEmail(
  username,
  password,
  geetestChallenge,
  geetestValidate,
  geetestSeccode
) {
  const data = {
    username,
    password,
    geetestChallenge,
    geetestValidate,
    geetestSeccode
  };
  return fetch({
    url: "/auth/jwt/token",
    method: "post",
    data
  });
}

export function logout(token) {
  return fetch({
    url: "/auth/jwt/invalid",
    method: "post",
    params: { token }
  });
}

export function getInfo(token) {
  return fetch({
    url: "/admin/user/front/info",
    method: "get",
    params: { token }
  });
}

export function getMenus(token) {
  return fetch({
    url: "/admin/user/front/menus",
    method: "get",
    params: { token }
  });
}

export function getAllMenus() {
  return fetch({
    url: "/admin/user/front/menu/all",
    method: "get"
  });
}
